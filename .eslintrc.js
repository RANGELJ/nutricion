module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/recommended"
    ],
    // required to lint *.vue files
    plugins: [
        'html', "vue",
    ],
    // add your custom rules here
    rules: {
        "indent": ["error", 4],
        "quotes": ["error", "double"],
        "semi": ["error", "always"],
        "no-console": [0, "allow"]
    },
    globals: {
    }
}
