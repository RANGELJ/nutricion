
const axios = require("axios");

/**
 * Factory function
 * @param {*} context Context de nuxt
 * @return {*}
 */
export default function(context) {
    const headers = {};
    let withCredentials = true;
    if (context) {
        if (process.server && context.req && context.req.headers.cookie) {
            headers.cookie = context.req.headers.cookie;
        } else {
            withCredentials = true;
        }
    } else {
        withCredentials = true;
    }
    headers["Content-Type"] = "application/json";

    /**
     * Function that executes a request to the given
     * graph server
     * @param {string} queryString query to be executed
     * @return {Promise}
     */
    return function(queryString) {
        return new Promise((resolve, reject) => {
            axios({
                // https://rangelsolutions.me/nutricionG
                url: "http://localhost/nutricionG",
                method: "POST",
                headers: headers,
                data: {
                    query: queryString,
                },
                withCredentials: withCredentials,
            })
                .then( (response) => {
                    response = response.data;
                    if (response.errors && response.errors.length > 0) {
                        const error = response.errors[0];
                        reject(error.message);
                    } else {
                        resolve(response.data);
                    }
                } ).catch( () => {
                    reject("Error de comunicacion");
                });
        });
    };
}
