
const fs = require("fs");

const pathToFiles = __dirname + "/../../graphqueries/";

const gqlPrefixRegex = "gpl@";
const variableRegex = "[a-zA-Z][a-zA-Z0-9]*";

const gqlRefereceRegexString
    = gqlPrefixRegex + variableRegex + "/" + variableRegex;

const gqlRefereceRegex = new RegExp(gqlRefereceRegexString, "g");

module.exports = function(source, map, meta) {
    const callback = this.async();

    const gqlReferences= source.match(gqlRefereceRegex);

    if (! gqlReferences) {
        callback(null, source, map, meta);
        return;
    }

    let newSource = source;

    gqlReferences.forEach(gqlReference => {
        const gqlPath = gqlReference.replace("gpl@", "");
        const gqlPaths = gqlPath.split("/");
        const filePath = pathToFiles +gqlPaths[0] + ".gql";
        const queryName = gqlPaths[1];

        const fileText = fs.readFileSync(filePath, "utf8");
        const queries = getQueries(fileText);
        const queryText = queries[queryName];

        newSource = newSource.replace(gqlReference, queryText);
    });

    callback(null, newSource, map, meta);

};

function getQueries(rawData) {
    const queryNameReguex = /-- @[a-zA-Z][a-zA-Z0-9]*/;
    const querySegment = /-- @[a-zA-Z][a-zA-Z0-9]*?[^-]+-- @/g;
    const namedQueries = rawData.match(querySegment);

    const queries = {};

    namedQueries.forEach(namedQuery => {
        let queryName = namedQuery.match(queryNameReguex)[0];
        queryName = queryName.replace("-- @", "");
        let queryBody = namedQuery.replace(queryNameReguex, "");
        queryBody = queryBody.replace("-- @", "");

        queries[queryName] = queryBody.replace(/\n/g, " ")
            .replace(/\s+/g, " ");
    });

    return queries;
}