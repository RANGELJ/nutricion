import Vue from "vue";
import graphFactory from "../local_modules/graphfactory";

Vue.use({
    install(Vue) {
        Vue.prototype.$graph = graphFactory();
    },
});
