
import Vue from "vue";
import NSpinner from "../components/NSpinner";
import NSelect from "../components/NSelect";
import NRDatePicker from "../components/NRDatePicker";
import NProcessToolbar from "../components/NProcessToolbar";
import NDialogConfirmacion from "../components/NDialogConfirmacion";

Vue.component("n-spinner", NSpinner);
Vue.component("n-select", NSelect);
Vue.component("n-process-toolbar", NProcessToolbar);
Vue.component("n-dialog-confirmacion", NDialogConfirmacion);
Vue.component("n-date-picker", NRDatePicker);