export default {
    methods: {
        errorMessage (message) {
            this.$store.commit("mostrarMensaje", {
                type: "error",
                mensaje: message
            });
        },
        warningMessage (message) {
            this.$store.commit("mostrarMensaje", {
                type: "warning",
                mensaje: message
            });
        },
        successMessage (message) {
            this.$store.commit("mostrarMensaje", {
                type: "success",
                mensaje: message
            });
        }
    }
};
