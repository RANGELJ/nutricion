import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _1d1b0c66 = () => import('../pages/reportes.vue' /* webpackChunkName: "pages/reportes" */).then(m => m.default || m)
const _2fb70bf2 = () => import('../pages/pacientes/consultas/indicadoresdieteticos/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/indicadoresdieteticos/_id" */).then(m => m.default || m)
const _3574f212 = () => import('../pages/pacientes/consultas/indicadoresantropometricos/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/indicadoresantropometricos/_id" */).then(m => m.default || m)
const _76937464 = () => import('../pages/pacientes/consultas/estilodevida/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/estilodevida/_id" */).then(m => m.default || m)
const _f2a8021c = () => import('../pages/pacientes/consultas/frecuenciaalimentos/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/frecuenciaalimentos/_id" */).then(m => m.default || m)
const _f580941c = () => import('../pages/pacientes/consultas/indicadoresclinicos/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/indicadoresclinicos/_id" */).then(m => m.default || m)
const _42a3fb70 = () => import('../pages/pacientes/consultas/_id.vue' /* webpackChunkName: "pages/pacientes/consultas/_id" */).then(m => m.default || m)
const _9765d890 = () => import('../pages/pacientes/editor_v2/_id.vue' /* webpackChunkName: "pages/pacientes/editor_v2/_id" */).then(m => m.default || m)
const _f3f1faba = () => import('../pages/historial_clinico.vue' /* webpackChunkName: "pages/historial_clinico" */).then(m => m.default || m)
const _80b5d804 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/nutricion/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/reportes",
			component: _1d1b0c66,
			name: "reportes"
		},
		{
			path: "/pacientes/consultas/indicadoresdieteticos/:id?",
			component: _2fb70bf2,
			name: "pacientes-consultas-indicadoresdieteticos-id"
		},
		{
			path: "/pacientes/consultas/indicadoresantropometricos/:id?",
			component: _3574f212,
			name: "pacientes-consultas-indicadoresantropometricos-id"
		},
		{
			path: "/pacientes/consultas/estilodevida/:id?",
			component: _76937464,
			name: "pacientes-consultas-estilodevida-id"
		},
		{
			path: "/pacientes/consultas/frecuenciaalimentos/:id?",
			component: _f2a8021c,
			name: "pacientes-consultas-frecuenciaalimentos-id"
		},
		{
			path: "/pacientes/consultas/indicadoresclinicos/:id?",
			component: _f580941c,
			name: "pacientes-consultas-indicadoresclinicos-id"
		},
		{
			path: "/pacientes/consultas/:id?",
			component: _42a3fb70,
			name: "pacientes-consultas-id"
		},
		{
			path: "/pacientes/editor:v2?/:id?",
			component: _9765d890,
			name: "pacientes-editorv2-id"
		},
		{
			path: "/historial:clinico",
			component: _f3f1faba,
			name: "historialclinico"
		},
		{
			path: "/",
			component: _80b5d804,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
