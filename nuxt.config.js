const nodeExternals = require("webpack-node-externals");
const path = require("path");
const resolve = (dir) => require("path").join(__dirname, dir);

module.exports = {

    /*
    ** Headers of the page
    */
    head: {
        title: "nutricion",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            { hid: "description", name: "description", content: "Version del lado del servidor de nutricion" }
        ],
        link: [
            { rel: "icon", type: "image/x-icon", href: "/main_icon.png" },
            { rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" }
        ]
    },
    router: {
        base: '/nutricion/'
    },
    plugins: [
        "~/plugins/vuetify.js",
        "~/plugins/graph.js",
        "~/plugins/global_components.js"
    ],
    css: [
        "~/assets/style/app.styl"
    ],
    /*
    ** Customize the progress bar color
    */
    loading: { color: "#3B8070" },
    /*
    ** Build configuration
    */
    build: {
        babel: {
            plugins: [
                ["transform-imports", {
                    "vuetify": {
                        "transform": "vuetify/es5/components/${member}",
                        "preventFullImport": true
                    }
                }]
            ]
        },
        vendor: [
            "~/plugins/vuetify.js",
            "~/plugins/graph.js",
            "~/plugins/global_components.js"
        ],
        extractCSS: true,
        /*
        ** Run ESLint on save
        */
        extend (config, ctx) {
            config.resolveLoader.alias = {
                "test_loader": path.join(__dirname, "local_modules", "loaders", "testLoader.js"),
            };
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    test: /\.(js|vue)$/,
                    loader: "eslint-loader",
                    exclude: /(node_modules)/
                });
            }

            if (ctx.isServer) {
                config.externals = [
                    nodeExternals({
                        whitelist: [/^vuetify/]
                    })
                ];
            }

            config.module.rules.push({
                test: /\.vue$/,
                loader: "test_loader",
                exclude: /node_modules/,
            });
        }
    }
};
